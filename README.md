# Specifikation för Anslagstavla

Information om anslagstavlor.

## Förstudie 

Förstudie arbetas fram på kodförrådets [wiki](https://gitlab.com/lankadedata/spec/anslagstavla/-/wikis/home).

Finansieras av [Nationell Dataverkstad](https://www.vgregion.se/ov/dataverkstad/).
